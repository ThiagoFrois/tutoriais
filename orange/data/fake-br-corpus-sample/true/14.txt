Raquel Dodge reitera pedido de Janot para rescindir delaÃ§Ã£o de Joesley Batista e Ricardo Saud
EmpresÃ¡rio e executivo do grupo J&F estÃ£o presos. Antecessor de Dodge considerou que os dois omitiram informaÃ§Ãµes e pediu ao STF a rescisÃ£o do acordo de delaÃ§Ã£o premiada.
A procuradora-geral da RepÃºblica, Raquel Dodge, enviou manifestaÃ§Ã£o ao Supremo Tribunal Federal (STF) reforÃ§ando um pedido para rescisÃ£o definitiva do acordo de delaÃ§Ã£o premiada firmado entre o Ã³rgÃ£o, o empresÃ¡rio Joesley Batista, do grupo J&F, e Ricardo Saud, executivo do grupo.

A colaboraÃ§Ã£o foi suspensa em setembro a pedido do antecessor de Dodge, Rodrigo Janot, por suposta omissÃ£o e mÃ¡-fÃ© dos delatores â eles teriam deixado de informar sobre suposta orientaÃ§Ã£o prestada pelo ex-procurador Marcello Miller nas negociaÃ§Ãµes, enquanto ainda integrava o MinistÃ©rio PÃºblico.

A decisÃ£o sobre a rescisÃ£o definitiva do acordo â que pode enterrar de vez os benefÃ­cios que haviam sido concedidos a Joesley e Saud â cabe ao ministro Edson Fachin, relator da OperaÃ§Ã£o Lava Jato no STF.

Nesta terÃ§a (19), o plenÃ¡rio da Corte deverÃ¡ discutir um pedido de liberdade apresentado pelos dois, presos desde setembro. Na sessÃ£o, os 11 ministros da Corte tambÃ©m poderÃ£o discutir a situaÃ§Ã£o da delaÃ§Ã£o.

ManifestaÃ§Ã£o de Dodge
No documento, Raquel Dodge refuta argumentos das defesas de Joesley e Saud para manter os acordos de colaboraÃ§Ã£o â os dois estÃ£o presos atualmente.

Os advogados dizem que foram os prÃ³prios delatores que entregaram a gravaÃ§Ã£o de uma conversa na qual falam sobre Miller, como complementaÃ§Ã£o Ã  delaÃ§Ã£o premiada.

AlÃ©m disso, negam mÃ¡-fÃ©, alegando que contrataram o ex-procurador como advogado â para ajudar no acordo de leniÃªncia da JBS â sem saber de qualquer impedimento, jÃ¡ que ele estava de saÃ­da do MinistÃ©rio PÃºblico Ã  Ã©poca, entre abril e maio deste ano.

Dodge diz que esses fatos sÃ³ chegaram ao conhecimento da PGR no dia 31 de agosto deste ano, quando o Ã¡udio da conversa entre Joesley e Saud foi entregue â tratava-se do prazo final para complementar a delaÃ§Ã£o.

Para ela, a colaboraÃ§Ã£o de Miller deveria ter sido informada desde o inÃ­cio das negociaÃ§Ãµes, no comeÃ§o deste ano.

âTrata-se, portanto, de forte indÃ­cio, e nÃ£o mera especulaÃ§Ã£o, a reforÃ§ar que os colaboradores agiram movidos pelo intuito de ludibriar o MPF e proteger aliados, em conduta claramente desleal e afrontosa ao pacto que fizeram nos Acordos de ColaboraÃ§Ã£o Premiada e Ã  justiÃ§aâ, diz Dodge na manifestaÃ§Ã£o enviada ao STF.